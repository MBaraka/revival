package com.revival.customViews;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.revival.R;
import com.revival.activities.BaseActivity;
import com.revival.fragments.RestaurantFragment;
import com.revival.models.Restaurant;
import com.revival.utilities.Logger;

import java.util.Random;


public class RestaurantView extends LinearLayout implements View.OnClickListener
{
    private Restaurant restaurant;
    TextView txt_title, txt_description, txt_rate, txt_distance;
    ImageView img;

    //region constructors
    public RestaurantView(Context context)
    {
        super(context);
        init(context);
    }

    public RestaurantView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context);
    }

    public RestaurantView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context)
    {
        ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.restaurant_view, this);
        txt_title = (TextView) findViewById(R.id.restaurantView_txt_title);
        txt_description = (TextView) findViewById(R.id.restaurantView_txt_description);
        txt_rate = (TextView) findViewById(R.id.restaurantView_txt_rate);
        txt_distance = (TextView) findViewById(R.id.restaurantView_txt_distance);
        img = (ImageView) findViewById(R.id.restaurantView_img);

    }

    //endregion


    public void setRestaurant(Restaurant restaurant)
    {
        this.restaurant = restaurant;

        txt_description.setText(restaurant.getAddress());
        txt_title.setText(restaurant.getName());
        txt_rate.setText(restaurant.getRating() + "/10");

        String distanceString = restaurant.getDistance();
        int distance = (int) Float.parseFloat(distanceString);
        txt_distance.setText(getResources().getString(R.string.inMeters, distance));

        if (restaurant.getFirstPhoto() != null)
        {
            setBackground(restaurant.getFirstPhoto());
        }
        else
        {
            setBackground(getRandomURL());
        }

        setOnClickListener(this);
    }

    public void setBackground(String url)
    {
        Glide.with(getContext()).load(url).asBitmap().into(new SimpleTarget<Bitmap>()
        {
            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable)
            {
                super.onLoadFailed(e, errorDrawable);
                Logger.e(e);
                setBackground(getRandomURL());
            }

            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation)
            {
                Drawable drawable = new BitmapDrawable(resource);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                {
                    img.setBackground(drawable);
                }
                else
                {
                    img.setBackgroundDrawable(drawable);
                }
            }
        });
    }

    private String getRandomURL()
    {
        //this is just random images to be used in case failed to load the restaurant images as a default image

        String[] myArrayOfStrings = {"https://pbs.twimg.com/profile_images/608377553808703489/2gBSCjiq.jpg",
                "https://www.coventgarden.london/sites/default/files/styles/cg_place_detail_1_1/public/cg_images/Detail%20Shot%20(2).jpg?itok=vPKcFuPL",
                "http://static.wixstatic.com/media/3e3e47_4cf4e3cffa03419a9c4d1dc5c07b21e8.jpg_256",
                "https://s-media-cache-ak0.pinimg.com/736x/56/29/d5/5629d529bbaf9894b047b0bf031b03bd.jpg"};

        return myArrayOfStrings[new Random().nextInt(myArrayOfStrings.length)];
    }

    @Override
    public void onClick(View view)
    {
        if (getContext() instanceof BaseActivity)
        {
            ((BaseActivity) getContext()).addContentFragment(RestaurantFragment.init(restaurant), true);
        }
    }
}
