package com.revival.customViews;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.revival.R;
import com.revival.utilities.Logger;

import java.util.List;

public class HideOverlayView extends LinearLayout
{
    private Bitmap windowFrame;
    private float radius = 0f;
    private List<Point> points;

    public HideOverlayView(Context context)
    {
        super(context);
    }

    public HideOverlayView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    @Override
    protected void dispatchDraw(Canvas canvas)
    {
        super.dispatchDraw(canvas);

        createWindowFrame();
        canvas.drawBitmap(windowFrame, 0, 0, null);
    }

    @Override
    public boolean isEnabled()
    {
        return false;
    }

    @Override
    public boolean isClickable()
    {
        return false;
    }

    public void reDraw(List<Point> points, float radius)
    {
        this.points = points;
        this.radius = radius;

        invalidate();
    }

    protected void createWindowFrame()
    {
        windowFrame = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas osCanvas = new Canvas(windowFrame);

        RectF outerRectangle = new RectF(0, 0, getWidth(), getHeight());
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(getResources().getColor(R.color.colorBlackTransparent));
        osCanvas.drawRect(outerRectangle, paint);

        if (radius > 0 && points != null)
        {
            for (Point point : points)
            {
                paint.setColor(getResources().getColor(R.color.colorTransparent));
                paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OUT));
                paint.setStyle(Paint.Style.FILL);
                osCanvas.drawCircle(point.x, point.y, radius, paint);
                Logger.i(getClass().getSimpleName(), " >>>>>>>> radius " + radius + "  points size " + points.size());
            }
        }
        Logger.i(getClass().getSimpleName(), ">>>>> Couldn't draw because there is no points or radius size");
    }

    @Override
    public boolean isInEditMode()
    {
        return true;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        super.onLayout(changed, l, t, r, b);
        windowFrame = null;
    }
}
