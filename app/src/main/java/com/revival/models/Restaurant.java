package com.revival.models;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;


import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;
import com.revival.utilities.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Restaurant
{
    //region fields
    private static final String TAG = Restaurant.class.getSimpleName();

    private final int maxWidth = 200;
    private final int maxHieght = 200;

    private String id;
    private double lat;
    private double lng;
    private String name;
    private boolean open_now;
    private List<String> photos;
    private int price_level;
    private float rating;
    private String address;
    private String iconURL;

    private String phone;
    private String twitter;
    private String facebook;
    private String categories;

    private final int sourceType;
    private String distance;

    //endregion

    public final static int SOURCE_GOOGLE = 0;
    public final static int SOURCE_FOUR_SQUARE = 1;

    public Restaurant(int sourceType)
    {
        this.sourceType = sourceType;
    }

    //region setting photos
    private void setPhotos(@NonNull JSONArray jsonArray)
    {
        if (photos == null)
        {
            photos = new ArrayList<>(jsonArray.length());
        }

        for (int i = 0; i < jsonArray.length(); i++)
        {
            try
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                photos.add(extractPhotoURL(jsonObject.getString("photo_reference")));
            }
            catch (JSONException e)
            {
                Logger.e(e);
            }

        }
    }

    private String extractPhotoURL(String reference)
    {
        return "https://maps.googleapis.com/maps/api/place/photo?" + "maxwidth=" + maxWidth + "&photoreference=" + reference;
    }
    //endregion

    //region getters
    public String getId()
    {
        return id;
    }

    public double getLat()
    {
        return lat;
    }

    public double getLng()
    {
        return lng;
    }

    public String getName()
    {
        return name;
    }

    public boolean isOpen_now()
    {
        return open_now;
    }

    public List<String> getPhotos()
    {
        return photos;
    }

    @Nullable
    public String getFirstPhoto()
    {
        if (photos != null && photos.size() > 0)
        {
            return photos.get(0);
        }
        return null;
    }

    public int getPrice_level()
    {
        return price_level;
    }

    public float getRating()
    {
        return rating;
    }

    public String getAddress()
    {
        return address;
    }

    @Nullable
    public String getPhone()
    {
        return phone;
    }

    public String getFacebook()
    {
        if (facebook != null)
        {
            return "http://facebook.com/profile.php?id=" + facebook;
        }
        else
        {
            return null;
        }
    }

    public String getTwitter()
    {
        return twitter;
    }

    /**
     * @return The source of the restaurant, it can be {@link #SOURCE_GOOGLE} or {@link #SOURCE_FOUR_SQUARE}
     */
    public int getSourceType()
    {
        return sourceType;
    }

    public String getCategories()
    {
        return categories;
    }

    public String getDistance()
    {
        return distance;
    }

    public void setDistance(String distance)
    {
        this.distance = distance;
    }

    //endregion

    //region getting restaurant(s)
    public static Restaurant getRestaurantGoogle(JSONObject jsonObject, LatLng latLng)
    {
        Restaurant restaurant = new Restaurant(SOURCE_GOOGLE);
        try
        {
            JSONObject geo = jsonObject.getJSONObject("geometry");
            geo = geo.getJSONObject("location");
            restaurant.lat = geo.getDouble("lat");
            restaurant.lng = geo.getDouble("lng");

            restaurant.id = jsonObject.getString("id");
            restaurant.name = jsonObject.getString("name");

            if (!jsonObject.isNull("vicinity"))
            {
                restaurant.address = jsonObject.getString("vicinity");
            }

            LatLng targetLocation = new LatLng(restaurant.getLat(), restaurant.getLng());
            restaurant.distance = String.valueOf(SphericalUtil.computeDistanceBetween(targetLocation, latLng));

            if (!jsonObject.isNull("opening_hours"))
            {
                JSONObject jsonObject1 = jsonObject.getJSONObject("opening_hours");
                restaurant.open_now = jsonObject1.getBoolean("open_now");
            }

            if (!jsonObject.isNull("price_level"))
            {
                restaurant.price_level = jsonObject.getInt("price_level");
            }

            if (!jsonObject.isNull("rating"))
            {
                restaurant.rating = jsonObject.getInt("rating");
            }

            if (!jsonObject.isNull("photos"))
            {
                restaurant.setPhotos(jsonObject.getJSONArray("photos"));
            }

            if (!jsonObject.isNull("icon"))
            {
                restaurant.iconURL = jsonObject.getString("icon");
            }

            if (!jsonObject.isNull("types"))
            {
                restaurant.categories = jsonObject.getJSONArray("types").toString();
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return restaurant;
    }

    public static Restaurant getRestaurantFourSquare(JSONObject jsonObject, LatLng latLng)
    {
        Restaurant restaurant = new Restaurant(SOURCE_FOUR_SQUARE);
        try
        {
            restaurant.id = jsonObject.getString("id");
            restaurant.name = jsonObject.getString("name");


            JSONObject location = jsonObject.getJSONObject("location");
            restaurant.lat = location.getDouble("lat");
            restaurant.lng = location.getDouble("lng");
            if (!location.isNull("address"))
            {
                restaurant.address = location.getString("address");
            }
            else if(!location.isNull("formattedAddress"))
            {
                restaurant.address = location.getJSONArray("formattedAddress").toString();
            }

            LatLng targetLocation = new LatLng(restaurant.getLat(), restaurant.getLng());
            restaurant.distance = String.valueOf(SphericalUtil.computeDistanceBetween(targetLocation, latLng));

            JSONObject contactJSON = jsonObject.getJSONObject("contact");
            if (!jsonObject.isNull("phone"))
            {
                restaurant.phone = contactJSON.getString("phone");
            }

            if (!jsonObject.isNull("twitter"))
            {
                restaurant.twitter = contactJSON.getString("twitter");
            }

            if (!jsonObject.isNull("facebook"))
            {
                restaurant.facebook = contactJSON.getString("facebook");
            }

            if (!jsonObject.isNull("icon"))
            {
                restaurant.iconURL = jsonObject.getString("icon");
            }

            if (!jsonObject.isNull("categories"))
            {
                JSONArray categoryArray = jsonObject.getJSONArray("categories");
                for (int i = 0; i < categoryArray.length(); i++)
                {
                    JSONObject jsonObject1 = categoryArray.getJSONObject(i);
                    if (i > 0)
                    {
                        restaurant.categories = jsonObject1.getString("name");
                    }
                    else
                    {
                        restaurant.categories = restaurant.categories + ", " + jsonObject1.getString("name");
                    }
                }
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return restaurant;
    }

    /**
     * get a restaurant from a list using its name
     *
     * @param restaurants restaurants list
     * @return the restaurant or null if not found
     */
    @Nullable
    public static Restaurant getRestaurant(String id, String sourceType, List<Restaurant> restaurants)
    {
        for (Restaurant restaurant : restaurants)
        {
            if (restaurant.getId().equalsIgnoreCase(id) && restaurant.getSourceType() == Integer.parseInt(sourceType))
            {
                return restaurant;
            }
        }
        return null;
    }

    //endregion

    //region equal and hash code functions
    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Restaurant that = (Restaurant) o;

        return sourceType == that.sourceType && id.equals(that.id);

    }

    @Override
    public int hashCode()
    {
        int result = id.hashCode();
        result = 31 * result + sourceType;
        return result;
    }

    //endregion

}
