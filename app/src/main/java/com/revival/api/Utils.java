package com.revival.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.concurrent.Callable;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Collection of Util methods combined in 1 class not to cause memory overheard.
 */
public class Utils
{
    //region Constructors

    private Utils()
    {
        throw new IllegalStateException("no instances.");
    }

    //endregion

    //region Null

    @NonNull
    public static <T> T checkNotNull(@Nullable T object, String name)
    {
        if (object == null)
        {
            throw new NullPointerException(name + " may not be null.");
        }
        return object;
    }

    //endregion

    //region Async

    /**
     * Executes {@code func0} in {@link Schedulers#newThread()} and forwards the output.
     *
     * @param func0 function to be executed in background.
     * @return An Observable which executes {@code func0} in background for each subscriber
     * and forwards the output.
     */
    @NonNull
    public static <T> Observable<T> async(@NonNull final Callable<T> func0)
    {
        checkNotNull(func0, "func0");
        return Observable.fromCallable(func0)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    //endregion

}
