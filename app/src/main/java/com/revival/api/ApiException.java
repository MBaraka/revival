package com.revival.api;

import android.support.annotation.NonNull;


import com.revival.utilities.Logger;

import org.json.JSONException;
import org.json.JSONObject;

public class ApiException extends RuntimeException
{
    public static int error_Invalid_or_expired_accessToken;
    //region Constants
    private final String LOG;
    private int code;


    private static String parseError(@NonNull JSONObject jsonObject)
    {
        try
        {
            return jsonObject.getString("message");
        }
        catch (JSONException e)
        {
            Logger.e(e);
            return "Couldn't parse error, please debug it";
        }
    }
    //endregion

    //region Fields

    //endregion

    //region Constructors

    public ApiException(@NonNull JSONObject jsonObject)
    {
        super(parseError(jsonObject));
        try
        {
            code = jsonObject.getInt("code");
        }
        catch (JSONException e)
        {
            code = -1;
            e.printStackTrace();
        }
        LOG = getClass().getSimpleName();

    }

    public ApiException(@NonNull String message)
    {
        super(message);
        code = -1;
        LOG = getClass().getSimpleName();
        Logger.e(LOG, message);
    }

    public int getCode()
    {
        return code;
    }

    //endregion

}
