package com.revival.api;

import android.support.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.concurrent.Callable;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import rx.Observable;
import rx.functions.Func1;

import static com.revival.api.Utils.async;
import static com.revival.api.Utils.checkNotNull;


public final class ApiClient
{
    //region Fields

    private static final ApiClient INSTANCE = new ApiClient();
    //    HttpUrl.Builder client = new HttpUrl.Builder()
    private final OkHttpClient client;

    //endregion

    //region Initialize

    public static ApiClient getInstance()
    {
        return INSTANCE;
    }

    private ApiClient()
    {
        client = new OkHttpClient.Builder()
                .addInterceptor(new ContentTypeInterceptor())
                .addInterceptor(new LoggingInterceptor())
                .build();
    }

    //endregion

    //region Request

    @NonNull
    public Observable<String> requestPost(@NonNull String url, @NonNull RequestBody requestBody)
    {
        return request(new Request.Builder().post(requestBody).url(url).build());
    }

    @NonNull
    public Observable<String> requestPut(@NonNull String url, @NonNull RequestBody requestBody)
    {
        return request(new Request.Builder().put(requestBody).url(url).build());
    }

    @NonNull
    public Observable<String> request(@NonNull String url)
    {
        return request(new Request.Builder().url(url).build());
    }

    @NonNull
    public Observable<String> request(@NonNull final Request request)
    {
        checkNotNull(request, "request");
        return async(new Callable<String>()
        {
            @Override
            public String call() throws Exception
            {
                Response response = client.newCall(request).execute();
                boolean isSuccessful = response.isSuccessful();
                return response.body().string();
            }
        }).flatMap(new Func1<String, Observable<String>>()
        {
            @Override
            public Observable<String> call(String s)
            {
                return Observable.just(s);

            }
        });
    }

    //endregion
}
