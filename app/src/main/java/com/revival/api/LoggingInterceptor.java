package com.revival.api;


import com.revival.utilities.Logger;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

final class LoggingInterceptor implements Interceptor
{
    @Override
    public Response intercept(Chain chain) throws IOException
    {
        Request request = chain.request();

        Logger.i("Api", request.toString());
        Logger.i("Api", request.headers().toString());

        Response response = chain.proceed(request);

        ResponseBody body = response.body();
        String responseString = body.string();

        Logger.i("Api", response.toString());
        Logger.i("Api", responseString);

        ResponseBody newBody = ResponseBody.create(body.contentType(), responseString);
        return response
                .newBuilder()
                .body(newBody)
                .build();
    }
}
