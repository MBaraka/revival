package com.revival.fragments;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.revival.R;
import com.revival.controllers.SearchRestaurantsController;
import com.revival.fragments.BaseFragment;
import com.revival.models.Restaurant;
import com.revival.utilities.Logger;

import java.util.Random;


public class RestaurantFragment extends BaseFragment implements View.OnClickListener
{
    Restaurant restaurant;
    private TextView txt_title, txt_categories, txt_address, txt_rate, txt_price, txt_opened, txt_phone, txt_facebook, txt_twitter;
    private ImageView imageView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        View mainView = inflater.inflate(R.layout.restaurant_fragment, container, false);

        txt_title = (TextView) mainView.findViewById(R.id.restaurantFragment_txt_title);
        txt_categories = (TextView) mainView.findViewById(R.id.restaurantFragment_txt_categories);

        txt_address = (TextView) mainView.findViewById(R.id.restaurantFragment_txt_address);
        txt_address.setOnClickListener(this);

        txt_rate = (TextView) mainView.findViewById(R.id.restaurantFragment_txt_rate);
        txt_price = (TextView) mainView.findViewById(R.id.restaurantFragment_txt_price);
        txt_opened = (TextView) mainView.findViewById(R.id.restaurantFragment_txt_opened);

        txt_phone = (TextView) mainView.findViewById(R.id.restaurantFragment_txt_phone);
        txt_facebook = (TextView) mainView.findViewById(R.id.restaurantFragment_txt_facebook);
        txt_twitter = (TextView) mainView.findViewById(R.id.restaurantFragment_txt_twitter);

        imageView = (ImageView) mainView.findViewById(R.id.restaurantFragment_img);

        return mainView;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (restaurant != null)
        {
            txt_title.setText(restaurant.getName());
            txt_categories.setText(restaurant.getCategories());

            txt_address.setText("Address is: " + restaurant.getAddress());
            txt_rate.setText("restaurant rate: " + restaurant.getRating());
            txt_price.setText("restaurant price rate: " + restaurant.getPrice_level());

            if (restaurant.isOpen_now())
            {
                txt_opened.setText("opened now");
            }
            else
            {
                txt_opened.setText("closed now");
            }

            if (restaurant.getFirstPhoto() != null)
            {
                setBackground(restaurant.getFirstPhoto());
            }
            else
            {
                setBackground(getRandomURL());
            }

            if (restaurant.getSourceType() == Restaurant.SOURCE_FOUR_SQUARE)
            {
                if (restaurant.getPhone() != null)
                {
                    txt_phone.setVisibility(View.VISIBLE);
                    txt_phone.setText("Contact number is: " + restaurant.getPhone());
                }
                else
                {
                    txt_phone.setVisibility(View.GONE);
                }

                if (restaurant.getFacebook() != null)
                {
                    txt_facebook.setVisibility(View.VISIBLE);
                    txt_facebook.setText("Facebook link: " + restaurant.getFacebook());
                }
                else
                {
                    txt_facebook.setVisibility(View.GONE);
                }

                if (restaurant.getTwitter() != null)
                {
                    txt_twitter.setVisibility(View.VISIBLE);
                    txt_twitter.setText("Twitter: " + restaurant.getTwitter());
                }
                else
                {
                    txt_twitter.setVisibility(View.GONE);
                }
            }
            else if(restaurant.getSourceType() == Restaurant.SOURCE_GOOGLE)
            {
                txt_phone.setVisibility(View.GONE);
                txt_twitter.setVisibility(View.GONE);
                txt_facebook.setVisibility(View.GONE);
            }

        }
    }

    //region init instance
    public static RestaurantFragment init(String id, String sourceType)
    {
        RestaurantFragment restaurantFragment = new RestaurantFragment();
        restaurantFragment.restaurant = Restaurant.getRestaurant(id, sourceType, SearchRestaurantsController.getSearchRestaurantsController().getRestaurantList());

        return restaurantFragment;
    }

    public static RestaurantFragment init(Restaurant restaurant)
    {
        RestaurantFragment restaurantFragment = new RestaurantFragment();
        restaurantFragment.restaurant = restaurant;

        return restaurantFragment;
    }
    //endregion


    public void setBackground(String url)
    {
        Glide.with(getContext()).load(url).asBitmap().into(new SimpleTarget<Bitmap>()
        {
            @Override
            public void onLoadFailed(Exception e, Drawable errorDrawable)
            {
                super.onLoadFailed(e, errorDrawable);
                Logger.e(e);
                setBackground(getRandomURL());
            }

            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation)
            {
                imageView.setImageBitmap(resource);
            }
        });
    }

    private String getRandomURL()
    {
        //this is just random images to be used in case failed to load the restaurant images as a default image

        String[] myArrayOfStrings = {"https://pbs.twimg.com/profile_images/608377553808703489/2gBSCjiq.jpg",
                "https://www.coventgarden.london/sites/default/files/styles/cg_place_detail_1_1/public/cg_images/Detail%20Shot%20(2).jpg?itok=vPKcFuPL",
                "http://static.wixstatic.com/media/3e3e47_4cf4e3cffa03419a9c4d1dc5c07b21e8.jpg_256",
                "https://s-media-cache-ak0.pinimg.com/736x/56/29/d5/5629d529bbaf9894b047b0bf031b03bd.jpg"};

        return myArrayOfStrings[new Random().nextInt(myArrayOfStrings.length)];
    }

    @Override
    public void onClick(View view)
    {
        if(view.getId() == txt_address.getId())
        {
            getBaseActivity().startNavigation(restaurant.getLat(), restaurant.getLng());
        }
        else if(view.getId() == txt_phone.getId())
        {
            getBaseActivity().callPhone(restaurant.getPhone());
        }
    }
}
