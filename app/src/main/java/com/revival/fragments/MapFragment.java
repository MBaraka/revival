package com.revival.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;
import com.revival.R;
import com.revival.adapters.MapPagerAdapter;
import com.revival.controllers.SearchRestaurantsController;
import com.revival.customViews.HideOverlayView;
import com.revival.models.Restaurant;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action1;
import rx.subjects.BehaviorSubject;

import static com.revival.controllers.SearchRestaurantsController.RADIUS_CIRCLE;

public class MapFragment extends BaseFragment
        implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMarkerClickListener, ViewPager.OnPageChangeListener, GoogleMap.OnMapClickListener, GoogleMap.OnCameraMoveListener
{
    //region fields
    protected MapView mapView;
    protected GoogleMap googleMap;
    protected final int permissionCode_location = 1;
    protected ViewPager viewPager;
    private MapPagerAdapter mapPagerAdapter;

    private String markerID;
    private HashMap<Restaurant, Marker> restaurantMarkerHashMap;
    private Marker currentMarker;
    private Marker marker_originPoint;

    private static final int ZOOM_SEARCH = 14;
    private static final float ZOOM_DEFAULT = 14.5f;
    private static final float STROKE_WIDTH = 6;

    private HideOverlayView hideView;
    //endregion

    //region life cycle
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        View mainView = inflater.inflate(R.layout.map_view, container, false);

        initMap(savedInstanceState, mainView);
        viewPager = (ViewPager) mainView.findViewById(R.id.mapView_viewPager);
        hideView = (HideOverlayView) mainView.findViewById(R.id.hideview);

        setHasOptionsMenu(true);
        getBehaviorSubject().subscribe(new Action1<Integer>()
        {
            @Override
            public void call(Integer zoom)
            {
                if(googleMap != null )
                {
                    CameraPosition cameraPosition = googleMap.getCameraPosition();
                    LatLng latLng = cameraPosition.target;
                    addCircleToMap(latLng);
                }
            }
        });
        return mainView;
    }

    /**
     * call this method to initiate the map, from method {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <br>
     * pass the view that you inflated and the bundle
     */
    protected void initMap(Bundle savedInstanceState, View mainView)
    {
        mapView = (MapView) mainView.findViewById(R.id.mapView_map);
        mapView.onCreate(savedInstanceState);

        try
        {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        mapView.getMapAsync(this);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (mapView == null)
        {
            throw new RuntimeException("call the method initMap in your onCreateView");
        }
        mapView.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mapView.onDestroy();

    }

    @Override
    public void onLowMemory()
    {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    //endregion

    //region map
    @Override
    public void onMapReady(GoogleMap map)
    {
        this.googleMap = map;
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnCameraMoveListener(this);
        activateMyLocation();

        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(getActivity());
        locationProvider.getLastKnownLocation()
                .subscribe(new Action1<Location>()
                {
                    @Override
                    public void call(Location location)
                    {
                        LatLng start = new LatLng(location.getLatitude(), location.getLongitude());

                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(start, ZOOM_DEFAULT);
                        googleMap.animateCamera(cameraUpdate);

                        requestRestaurants(new LatLng(location.getLatitude(), location.getLongitude()));

                    }
                });


    }

    private void addCircle(LatLng latLng)
    {
        googleMap.addCircle(new CircleOptions()
                .center(latLng)
                .radius(RADIUS_CIRCLE)
                .strokeWidth(STROKE_WIDTH)
                .strokeColor(getResources().getColor(R.color.colorBlackTransparent))
                .fillColor(getResources().getColor(R.color.colorTransparent)));

    }

    /**
     * add moving circle with
     */
    private void addCircleToMap(LatLng latLng)
    {
        // draw circle
//        int d = 500; // diameter
//        Bitmap bm = Bitmap.createBitmap(d, d, Bitmap.Config.ARGB_8888);
//        Canvas c = new Canvas(bm);
//        Paint p = new Paint();
//        p.setColor(getResources().getColor(R.color.colorTransparent));
//        c.drawCircle(d / 2, d / 2, d / 2, p);

        // generate BitmapDescriptor from circle Bitmap
//        BitmapDescriptor bmD = BitmapDescriptorFactory.fromBitmap(bm);
//
//        // mapView is the GoogleMap
//        googleMap.addGroundOverlay(new GroundOverlayOptions().
//                image(bmD).
//                position(latLng, RADIUS_CIRCLE * 2, RADIUS_CIRCLE * 2).
//                transparency(0.4f));
        if(latLng != null && marker_originPoint != null)
        {
            if(hideView != null && hideView.getVisibility() != View.VISIBLE)
                hideView.setVisibility(View.VISIBLE);

            List<Point> visiblePoints = new ArrayList<>();
            Projection projection = googleMap.getProjection();

            visiblePoints.add((projection.toScreenLocation(latLng)));

            float radius = 1500f; // meters
//        CameraPosition cameraPosition = googleMap.getCameraPosition();
            Point centerPoint = projection.toScreenLocation(latLng);
            Point radiusPoint = projection.toScreenLocation(SphericalUtil.computeOffset(latLng, radius, 90));

            float radiusPx = (float) Math.sqrt(Math.pow(centerPoint.x - radiusPoint.x, 2));

            hideView.reDraw(visiblePoints, radiusPx);
        }
    }
    //endregion

    //region permission result
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case permissionCode_location:
            {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    activateMyLocation();
                }
                else
                {
                    showMessage("Permission not granted");
                }
            }

        }
    }

    //endregion

    //region my location clicked
    private void activateMyLocation()
    {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}
                    , permissionCode_location);
            return;
        }
        this.googleMap.setMyLocationEnabled(true);
        this.googleMap.setOnMapClickListener(this);
        this.googleMap.setOnMyLocationButtonClickListener(this);
    }

    @Override
    public boolean onMyLocationButtonClick()
    {
        return false;
    }
    //endregion

    //region request restaurants
    private void requestRestaurants(LatLng latLng)
    {
        showLoading(true);
        SearchRestaurantsController.getSearchRestaurantsController().requestRestaurants(getActivity(), latLng).subscribe(new Subscriber<List<Restaurant>>()
        {
            @Override
            public void onCompleted()
            {
                showLoading(false);
                googleMap.clear();

                marker_originPoint = null;
                addOriginMarker(SearchRestaurantsController.getSearchRestaurantsController().getLatLng());

//                addCircle(SearchRestaurantsController.getSearchRestaurantsController().getLatLng());


                List<Restaurant> restaurants = SearchRestaurantsController.getSearchRestaurantsController().getRestaurantList();
                if (restaurants.size() > 0)
                {
                    if (restaurantMarkerHashMap == null)
                    {
                        restaurantMarkerHashMap = new HashMap<>(restaurants.size());
                    }
                    else
                    {
                        restaurantMarkerHashMap.clear();
                    }

                    for (Restaurant restaurant : restaurants)
                    {
                        Marker marker = googleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(restaurant.getLat(), restaurant.getLng()))
                                .snippet(restaurant.getSourceType() + "")
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_black))
                                .title(restaurant.getId()));

                        restaurantMarkerHashMap.put(restaurant, marker);

                    }
                    mapPagerAdapter = new MapPagerAdapter(getActivity(), restaurants);
                    viewPager.addOnPageChangeListener(MapFragment.this);
                    viewPager.setAdapter(mapPagerAdapter);

                }
                else
                {
                    viewPager.setAdapter(null);
                    showMessage(R.string.noResult);
                }
            }

            @Override
            public void onError(Throwable e)
            {
                showLoading(false);
                showMessage(e.getLocalizedMessage());
            }

            @Override
            public void onNext(List<Restaurant> restaurants)
            {

            }
        });
    }

    //endregion

    //region marker clicked
    @Override
    public boolean onMarkerClick(Marker marker)
    {
        if(marker.getId().equals(marker_originPoint.getId()))
        {
            showAlertDialog(marker_originPoint.getPosition());
        }
        else
        {
            String id = marker.getTitle();
            String sourceType = marker.getSnippet();

            //show pager and slide to the correct one
            if (markerID == null || markerID.equals(marker.getId()))
            {
                slideUpDown();
            }
            else if (markerID != null && !isPanelShown())
            {
                slideUpDown();
            }
            markerID = marker.getId();

            Restaurant restaurant = Restaurant.getRestaurant(id, sourceType, SearchRestaurantsController.getSearchRestaurantsController().getRestaurantList());
            setCurrentItem(restaurant);
        }
//        BitmapDescriptor image = BitmapDescriptorFactory.fromResource(R.drawable.black);
//        GroundOverlayOptions groundOverlay = new GroundOverlayOptions()
//                .image(image)
//                .position(marker.getPosition(), 50000f)
//                .visible(true)
//                .transparency(0.5f);
//        googleMap.addGroundOverlay(groundOverlay);

        return true;
    }

    private void setCurrentItem(@NonNull Restaurant restaurant)
    {
        if (restaurant == null)
        {
            return;
        }

        int position = SearchRestaurantsController.getSearchRestaurantsController().getRestaurantList().indexOf(restaurant);
        if (position > -1)
        {
            viewPager.setCurrentItem(position);
        }
    }

    //endregion

    //region adjust marker for a restaurant

    /**
     * adjust a marker by its position in the list of restaurants
     */
    private void adjustMarker(int position)
    {
        Restaurant restaurant = SearchRestaurantsController.getSearchRestaurantsController().getRestaurantList().get(position);
        adjustMarker(restaurantMarkerHashMap.get(restaurant));
    }

    /**
     * adjust specific marker to be blue colored while making previous marker black
     */
    private void adjustMarker(Marker marker)
    {
        marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.marker_blue));

        if (currentMarker != null && currentMarker != marker)
        {
            currentMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.marker_black));
        }

        currentMarker = marker;
    }

    private void addOriginMarker(LatLng latLng)
    {
        if (marker_originPoint == null)
        {
            marker_originPoint = googleMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title("Search here")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_red)));
        }
        else
        {
            if(!marker_originPoint.isVisible())
                marker_originPoint.setVisible(true);
            marker_originPoint.setPosition(latLng);
        }
    }
    //endregion


    //region slide up and down for view pager
    public void slideUpDown()
    {
        if (!isPanelShown())
        {
            // Show the panel
            Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.bottom_up);

            viewPager.startAnimation(bottomUp);
            viewPager.setVisibility(View.VISIBLE);
        }
        else
        {
            // Hide the Panel
            Animation bottomDown = AnimationUtils.loadAnimation(getActivity(), R.anim.bottom_down);

            viewPager.startAnimation(bottomDown);
            viewPager.setVisibility(View.GONE);
        }
    }

    private boolean isPanelShown()
    {
        return viewPager.getVisibility() == View.VISIBLE;
    }

    //endregion

    //region menu
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_map_fragment, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = new SearchView(getActivity());
        MenuItemCompat.setActionView(item, searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                //TODO we can search for one
                Restaurant restaurant = SearchRestaurantsController.getSearchRestaurantsController().search(query);
                if (restaurant != null)
                {
                    if (!isPanelShown())
                    {
                        slideUpDown();
                    }
                    setCurrentItem(restaurant);
                    adjustMarker(restaurantMarkerHashMap.get(restaurant));
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText)
            {
                return false;
            }
        });

        searchView.setOnClickListener(new View.OnClickListener()
                                      {
                                          @Override
                                          public void onClick(View v)
                                          {

                                          }
                                      }
        );
    }

    //endregion

    //region pager listener
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
    {

    }

    @Override
    public void onPageSelected(int position)
    {
        adjustMarker(position);
    }

    @Override
    public void onPageScrollStateChanged(int state)
    {

    }

    //endregion

    //region alert to change search position
    private void showAlertDialog(final LatLng latLng)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        alertDialogBuilder
                .setMessage("Are you sure you want to search around this new location? This will clear the previous data!")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        // if this button is clicked, close
                        // current activity
                        requestRestaurants(latLng);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }

    @Override
    public void onMapClick(LatLng latLng)
    {
        showAlertDialog(latLng);
    }
    //endregion

    @Override
    public void onCameraMove()
    {
        setZoom();
        CameraPosition cameraPosition = googleMap.getCameraPosition();

        if ( (int)cameraPosition.zoom > ZOOM_SEARCH)
        {
            LatLng latLng = cameraPosition.target;
            addOriginMarker(latLng);
//            addCircle(latLng);
        }

    }


    BehaviorSubject<Integer> behaviorSubject;
    private void setZoom()
    {
        CameraPosition cameraPosition = googleMap.getCameraPosition();
        if(behaviorSubject == null)
            behaviorSubject = BehaviorSubject.create((int)cameraPosition.zoom);
        else
            behaviorSubject.onNext((int)cameraPosition.zoom);
    }

    public Observable<Integer> getBehaviorSubject()
    {
        if(behaviorSubject == null)
            behaviorSubject = BehaviorSubject.create();
        return behaviorSubject.asObservable().throttleFirst(500, TimeUnit.MILLISECONDS);
    }
}
