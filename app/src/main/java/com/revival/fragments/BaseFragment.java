package com.revival.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.revival.activities.BaseActivity;
import com.trello.rxlifecycle.components.support.RxFragment;


public abstract class BaseFragment extends RxFragment
{
    public final String TAG = this.getClass().getSimpleName();

    /**
     * Method to add fragment from a fragment.
     *
     * @param fragment the fragment to be added
     * @param addToBackStack keep track of fragments been added. true to track other wise pass false.
     */
    public void addFragment(Fragment fragment, boolean addToBackStack)
    {
        if (getActivity() != null)
        {
            ((BaseActivity) getActivity()).addContentFragment(fragment, addToBackStack);
        }
    }

    //region life cycles
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    //endregion

    /**
     * show\hide loading which is in title bar
     */
    public void showLoading(boolean loading)
    {
        if (getActivity() == null)
            return;

        ((BaseActivity) getActivity()).showLoading(loading);
    }

    /**
     * this method will return baseActivity only if it is instanceOf {@link BaseActivity}
     *
     * @return {@link BaseActivity}
     * throws NullPointerException
     */
    protected BaseActivity getBaseActivity() throws NullPointerException
    {
        if (getActivity() instanceof BaseActivity)
            return (BaseActivity) getActivity();
        else
            return null;
    }

    protected void showMessage(int stringID)
    {
        Toast.makeText(getActivity(), getString(stringID), Toast.LENGTH_SHORT).show();
    }

    protected void showMessage(String text)
    {
        Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
    }


    protected void setTitle(String title)
    {
        getBaseActivity().setTitle(title);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }
}
