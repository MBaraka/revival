package com.revival.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.revival.customViews.RestaurantView;
import com.revival.models.Restaurant;

import java.util.List;


public class MapPagerAdapter extends PagerAdapter
{
    private final Context context;
    private List<Restaurant> restaurantList;

    public MapPagerAdapter(Context context, List<Restaurant> restaurantList)
    {
        this.context = context;
        this.restaurantList = restaurantList;
    }


    @Override
    public int getCount()
    {
        if (restaurantList != null)
        {
            return restaurantList.size();
        }
        return 0;
    }


    @Override
    public RestaurantView instantiateItem(ViewGroup container, int position)
    {
        RestaurantView restaurantView = new RestaurantView(context);
        restaurantView.setRestaurant(restaurantList.get(position));

        container.addView(restaurantView);

        return restaurantView;
    }

    @Override
    public boolean isViewFromObject(View view, Object object)
    {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view)
    {
        collection.removeView((View) view);
    }
}
