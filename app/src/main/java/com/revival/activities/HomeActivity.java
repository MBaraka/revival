package com.revival.activities;

import android.os.Bundle;

import com.revival.fragments.MapFragment;

public class HomeActivity extends BaseActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addContentFragment(new MapFragment(), false);
    }
}
