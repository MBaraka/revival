package com.revival.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.revival.R;
import com.revival.utilities.Logger;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

import java.util.List;

public class BaseActivity extends RxAppCompatActivity
{
    //region variables
    public final String content_Fragment = "Content_Fragment";

    protected static String TAG;

    protected ProgressBar mainProgressBar;
    private Toolbar toolbar;

    //endregion

    //region life cycle
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
        mainProgressBar = (ProgressBar) findViewById(R.id.homeActivity_progressBar);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Revival");
        setSupportActionBar(toolbar);


        TAG = this.getClass().getSimpleName();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
//		super.onSaveInstanceState(outState);
    }

    //endregion

    //region keyboard handlers

    public void hideVirtualKeyBoard()
    {
        try
        {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//			if(getCurrentFocus() != null)
//			{
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//			}
        }
        catch (NullPointerException mException)
        {
            Logger.i(this.getClass().getSimpleName(), "null pointer to hide keyboard");
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void showVirtualKeyBoard()
    {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    //endregion

    //region helper methods

    /**
     * method to insert Fragments in ContentFragment
     *
     * @param fragment       Fragment to be addded
     * @param addToBackStack record the transaction or not. True to record false other wise.
     */
    public void addContentFragment(Fragment fragment, boolean addToBackStack)
    {
        hideVirtualKeyBoard();
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.homeActivity_container);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.homeActivity_container, fragment);

        if (addToBackStack)
        {
//            if ((currentFragment != null && !currentFragment.getClass().isAssignableFrom(fragment.getClass()))
//                    || currentFragment == null)
            {
                fragmentTransaction.addToBackStack(content_Fragment);
            }
        }
        try
        {
            fragmentTransaction.commit();
        }
        catch (Exception ex)
        {
            Logger.e(ex);
        }
    }

    /**
     * clear backStack for the fragments and go back to the first fragment in the application
     */
    public void clearBackStack()
    {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0)
        {
//			FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
            manager.popBackStack(content_Fragment, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void callPhone(@NonNull String phone)
    {
//        Intent callIntent = new Intent(Intent.ACTION_CALL);
//        callIntent.setController(Uri.parse("tel:" + phone));
        if (phone != null)
        {
            Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
            if (ActivityCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
            {
                showMessage("No permission to do the phone call");
                return;
            }
            startActivity(callIntent);
        }
    }

    public void showLoading(boolean loading)
    {
        if (mainProgressBar == null)
        {
            return;
        }
        if (loading)
        {
            mainProgressBar.setVisibility(View.VISIBLE);
        }
        else
        {
            mainProgressBar.setVisibility(View.GONE);
        }
    }

    //endregion

    //region show message
    public void showMessage(@StringRes int resID)
    {
        showMessage(getString(resID));
    }

    public void showMessage(String text)
    {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
//        Snackbar.make(findViewById(R.id.homeActivity_container), text, Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show();
    }
    //endregion

    //region for permissions
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //report to the fragments
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null)
        {
            for (Fragment fragment : fragments)
            {
                fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }

    }

    //endregion

    public void startNavigation(double lat, double lng)
    {
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + lat + "," + lng);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

}
