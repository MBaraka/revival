package com.revival.utilities;

import android.util.Log;

public class Logger
{
    public static void i(String tag, String message)
    {
        if(tag == null || message == null)
        {
            return;
        }
//        if(BuildConfig.DEBUG)
        {
            Log.i(tag, message);
        }
    }

    public static void e(String tag, String message)
    {
        if(tag == null || message == null)
        {
            return;
        }
//        if(BuildConfig.DEBUG)
        {
            Log.e(tag, message);
        }
    }

    public static void d(String tag, String message)
    {
        if(tag == null || message == null)
        {
            return;
        }
//        if(BuildConfig.DEBUG)
        {
            Log.d(tag, message);
        }
    }

    public static void logException(Exception exception)
    {
        if(exception == null)
        {
            return;
        }
//        if(BuildConfig.DEBUG)
        {
            exception.printStackTrace();
        }
    }

    public static void e(Throwable throwable)
    {
        if(throwable != null )//&& BuildConfig.DEBUG)
            throwable.printStackTrace();
    }
}