package com.revival.controllers;

import android.content.Context;
import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.revival.api.ApiClient;
import com.revival.models.Restaurant;
import com.revival.utilities.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Request;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;
import rx.functions.Func1;


public class SearchRestaurantsController
{
    //region fields
    private List<Restaurant> restaurantList;
    private static final int RADIUS = 500;//Adapt this to the application
    public static final int RADIUS_CIRCLE = RADIUS * 20;//this is value can be used to draw a circle on the map

    private static final String Google_API_KEY = "AIzaSyBJIGyKaIFunvtlXZaGsbp5_nU4GqZZFds";
    private LatLng latLng;
    //endregion

    private SearchRestaurantsController()
    {
        restaurantList = new ArrayList<>();
    }

    /**
     * This function will request user location, unless a location is provided, then will search for near restaurants that is near to the location
     */
    public Observable<List<Restaurant>> requestRestaurants(Context context, LatLng latLng)
    {
        if (this.latLng != latLng && restaurantList != null)
        {
            restaurantList.clear();
        }

        this.latLng = latLng;

        return getCurrentLocation(context, latLng).flatMap(new Func1<Location, Observable<List<Restaurant>>>()
        {
            @Override
            public Observable<List<Restaurant>> call(Location location)
            {
                SearchRestaurantsController.this.latLng = new LatLng(location.getLatitude(), location.getLongitude());

                //TODO activate this to request more from FourSquare APIs
                return requestGoogle(location).mergeWith(requestFourSquare(location));
//                return requestGoogle(location);
//                return requestFourSquare(location);
            }
        });
    }

    /**
     * This will search for nearby restaurants using Google APIs
     */
    private Observable<List<Restaurant>> requestGoogle(final Location location)
    {
        String BASE_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
        String googleURL = BASE_URL + "location=" + location.getLatitude() + "," + location.getLongitude() + "&" + "radius" + "=" + RADIUS
                + "&type=restaurant&key=" + Google_API_KEY;

        Request request = new Request.Builder().url(googleURL).build();

        return ApiClient.getInstance()
                .request(request)
                .map(new Func1<String, List<Restaurant>>()
                {
                    @Override
                    public List<Restaurant> call(String response)
                    {
                        try
                        {
                            JSONObject responseJSON = new JSONObject(response);
                            JSONArray jsonArray = responseJSON.getJSONArray("results");
                            for (int i = 0; i < jsonArray.length(); i++)
                            {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                Restaurant restaurant = Restaurant.getRestaurantGoogle(jsonObject, new LatLng(location.getLatitude(), location.getLongitude()));

                                addRestaurant(restaurant);
                            }

                        }
                        catch (JSONException e)
                        {
                            Logger.e(e);
                        }

                        return restaurantList;
                    }
                });
    }

    /**
     * This will search for nearby restaurants using FourSquare APIs
     */
    private Observable<List<Restaurant>> requestFourSquare(final Location location)
    {
        String BASE_URL = "https://api.foursquare.com/v2/venues/search?";
        String fourSquareURL = BASE_URL + "ll=" + location.getLatitude() + "," + location.getLongitude()
                + "&oauth_token=ISSEIPO33BYTZQ55VQLXQXG05SQR3ZTVLIQENYNZLD4AR0S1&v=20170218&radius=" + RADIUS;


        Request request = new Request.Builder().url(fourSquareURL).build();

        return ApiClient.getInstance()
                .request(request)
                .map(new Func1<String, List<Restaurant>>()
                {
                    @Override
                    public List<Restaurant> call(String response)
                    {
                        try
                        {
                            JSONObject responseJSON = new JSONObject(response);
                            responseJSON = responseJSON.getJSONObject("response");
                            JSONArray jsonArray = responseJSON.getJSONArray("venues");
                            for (int i = 0; i < jsonArray.length(); i++)
                            {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                Restaurant restaurant = Restaurant.getRestaurantFourSquare(jsonObject, new LatLng(location.getLatitude(), location.getLongitude()));

                                addRestaurant(restaurant);
                            }

                        }
                        catch (JSONException e)
                        {
                            Logger.e(e);
                        }

                        return restaurantList;
                    }
                });
    }

    //region get current location
    private Observable<Location> getCurrentLocation(Context context, LatLng latLng)
    {
        if (latLng != null)
        {
            Location location = new Location("");
            location.setLatitude(latLng.latitude);
            location.setLongitude(latLng.longitude);
            return Observable.just(location);
        }
        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(context);
        return locationProvider.getLastKnownLocation();
    }
    //endregion

    //region list of restaurants
    public List<Restaurant> getRestaurantList()
    {
        return restaurantList;
    }

    private void addRestaurant(Restaurant restaurant)
    {
        if (!restaurantList.contains(restaurant))
        {
            restaurantList.add(restaurant);
        }
    }
    //endregion

    //region single instance
    private static SearchRestaurantsController searchRestaurantsController;

    public static SearchRestaurantsController getSearchRestaurantsController()
    {
        if (searchRestaurantsController == null)
        {
            searchRestaurantsController = new SearchRestaurantsController();
        }
        return searchRestaurantsController;
    }

    //endregion


    /**
     * This is the LatLng that is used as a center point while searching for nearby places
     */
    public LatLng getLatLng()
    {
        return latLng;
    }


    //region search

    /***
     * Search in the current list of restaurants if the name contains the given text
     * @return The first {@link Restaurant} that contain the given text
     */
    public Restaurant search(String text)
    {
        for(Restaurant restaurant: restaurantList)
        {
            if(restaurant.getName() != null && restaurant.getName().toLowerCase().contains(text))
            {
                return restaurant;
            }
        }
        return null;
    }
    //endregion

}
